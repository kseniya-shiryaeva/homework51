import {Component} from "react";
import './App.css';
import LotteryBall from "./components/LotteryBall/LotteryBall";

class App extends Component {
    state = {
        numberArray: []
    };

    createNumberArray = () => {
        const numberArray = [];
        while (true) {
            const number = Math.floor(Math.random() * 31) + 5;
            if (numberArray.includes(number)) {
                continue;
            } else {
                numberArray.push(number);
            }
            if(numberArray.length === 5) break;
        }
        numberArray.sort((a, b) => a > b ? 1 : -1);

        this.setState({numberArray});
    };

    render() {
        let balls = '';
        if (this.state.numberArray.length === 5) {
            balls = this.state.numberArray.map((item) => {
                return <LotteryBall number={item} />;
            });
        }

        return (
            <div className="container">
                <div className="button-block">
                    <button onClick={this.createNumberArray}>New numbers</button>
                </div>
                {balls}
            </div>
        );
    };
}

export default App;
