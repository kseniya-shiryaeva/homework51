import React from 'react';
import './LotteryBall.css';

const LotteryBall = props => {
    return (
        <div className="ball">
            {props.number}
        </div>
    );
};

export default LotteryBall;

